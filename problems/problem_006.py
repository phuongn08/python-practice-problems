# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


# def function (age, signed):
#     if age >= 18: 
#         return True
#     elif: 
#         consent == True
#         then we do this 
#     else: "sorry you can't skydive today"

# def can_skydive(age, consent):
#     if age >= 18 or consent == True:
#         print("You can go skydiving")
#     else: 
#         print("You cant go skydiving")
# can_skydive(30, False)

def can_skydive(age, has_consent_form):
    if age >= 18 or has_consent_form == True:
        return True
print(can_skydive(16, False))

# Planning

## Research

# * [ ] Vocab
# * [can_skydive function ] Functions
# * [ ] Methods

## Problem decomposition

# * [ age, signed] Input
# * [ return] Output
# * [ ] Examples
# * [greater than ] Conditions (if)
# * [ ] Iteration (loop)

## Problems

### 01 minimum_value

# Add plans...

### 04 max_of_three

### 06 can_skydive

### continue to plan each

# def can_skydive(age, has_consent_form):
#     pass

