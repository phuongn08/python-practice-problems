# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# Planning
# if divisible by 3 return the string "fizz"
# else return the original number input

def divisible_by_3(number):
    if number % 3 == 0:
        return "fizz"
    else:
        return number
print(divisible_by_3(10))
## Research
#for reference if something is modulo(%) by two it's an even number
# modulo % is the remainder of a/b

# * [ ] Vocab
# * [ ] Functions
# * [ ] Methods

# ## Problem decomposition

# * [ ] Input
# * [ ] Output
# * [ ] Examples
# * [ ] Conditions (if)
# * [ ] Iteration (loop)


# ## Problems

# ### 01 minimum_value

# Add plans...

# ### 04 max_of_three

# ### 06 can_skydive

# ### continue to plan each


# def is_divisible_by_3(number):
#     pass
