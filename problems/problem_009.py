# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# Planning
#we want to be able to reverse strings
#if the word backwards is the same 
## Research

# * [ ] Vocab
# * [ slice text = "hello world"[::-1] is_palindrome] Functions
# * [ .join, ] Methods
# myTuple = ("John", "Peter", "Vicky")

# x = "".join(myTuple)

# print(x)

## Problem decomposition

# def is_palindrome():
#     new_word = "".join(word)
#     if word == word[::-1]:
#         return True
#     else:


# * [] Input
# * [if word the same backwards and forwards = true] Output
# * [ racecar backwards is racecar which is true] Examples
# * [if not return false] Conditions (if)
#   if word == word[::-1] then true
# * [] Iteration (loop)

def is_palindrome(word):
    new_word = "".join(word)
    if new_word == new_word[::-1]:
        return True 

print(is_palindrome("racecar"))

# def is_palindrome(word):
#     pass


# text = "hello world"[::-1]
# print(text)